﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CITICSDBLoader
{
    static class Program
    {[STAThread]
        static void Main(string[] args)
        {
            Application.Run(new frmMain());
        }
    }
}
