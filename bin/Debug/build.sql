IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'Ticker')
BEGIN
CREATE TABLE Ticker
(
	ticker_id INT IDENTITY(1,1) PRIMARY KEY
	,symbol VARCHAR(99) NOT NULL
	,full_name VARCHAR(99) NOT NULL
)
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'Customer') 
BEGIN 
	CREATE TABLE Customer
	(
		customer_id INT IDENTITY(1,1) PRIMARY KEY
		,email_address VARCHAR(255) UNIQUE NOT NULL
		,password VARCHAR(255) NOT NULL
		,full_name VARCHAR(255) NOT NULL
		,last_login DATETIME DEFAULT(SYSDATETIME()) NOT NULL
	)
	INSERT [dbo].[Customer] ( [email_address], [password], [full_name], [last_login]) VALUES (N'mauris@dictumProineget.org', N'ligula.', N'Demetria Kennedy', CAST(N'2016-01-04 09:30:00.000' AS DateTime))
	INSERT [dbo].[Customer] ([email_address], [password], [full_name], [last_login]) VALUES (N'dolor@SuspendisseduiFusce.edu', N'neque.', N'Amela Becker', CAST(N'2016-01-07 09:45:00.000' AS DateTime))
	INSerT [dbo].[Customer] ( [email_address], [password], [full_name], [last_login]) VALUES (N'sagittis.semper@Aeneansedpede.edu', N'Donec', N'Helen Cruz', CAST(N'2016-03-04 10:30:00.000' AS DateTime))
	INSERT [dbo].[Customer] ([email_address], [password], [full_name], [last_login]) VALUES (N'cursus.diam.at@enimSednulla.com', N'enim.', N'Jael Gaines', CAST(N'2016-02-10 12:30:00.000' AS DateTime))
	INSERT [dbo].[Customer] ( [email_address], [password], [full_name], [last_login]) VALUES (N'ultrices.posuere@enim.ca', N'Proin', N'Ginger Day', CAST(N'2016-01-10 14:30:00.000' AS DateTime))
	INSERT [dbo].[Customer] ( [email_address], [password], [full_name], [last_login]) VALUES (N'massa@malesuada.com', N'in', N'Madison Ramos', CAST(N'2016-03-15 15:30:00.000' AS DateTime))
	INSERT [dbo].[Customer] ([email_address], [password], [full_name], [last_login]) VALUES (N'orci@accumsaninterdumlibero.net', N'bibendum', N'Ayanna Estes', CAST(N'2016-02-20 16:30:00.000' AS DateTime))
	INSERT [dbo].[Customer] ( [email_address], [password], [full_name], [last_login]) VALUES (N'pellentesque.eget.dictum@idmagna.ca', N'tellus', N'Genevieve Riggs', CAST(N'2016-01-25 12:30:00.000' AS DateTime))
	INSERT [dbo].[Customer] ( [email_address], [password], [full_name], [last_login]) VALUES (N'congue.elit.sed@et.edu', N'risus,', N'Raven Holland', CAST(N'2016-03-10 13:30:00.000' AS DateTime))
	INSERT [dbo].[Customer] ( [email_address], [password], [full_name], [last_login]) VALUES (N'nec.ligula.consectetuer@odioauctor.com', N'augue', N'Odessa Harrington', CAST(N'2016-02-11 10:30:00.000' AS DateTime))
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'Portfolio')
BEGIN
CREATE TABLE Portfolio
(
	portfolio_id INT IDENTITY(1,1) PRIMARY KEY
	,owner_id INT FOREIGN KEY (owner_id) REFERENCES Customer(customer_id)
	,name VARCHAR(255) NOT NULL
	,isPrivate BIT NOT NULL DEFAULT(1)
)
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'AggregateLookup')
BEGIN
CREATE TABLE AggregateLookup
(
	aggregate_id INT IDENTITY(1,1) PRIMARY KEY
	,name VARCHAR(99) NOT NULL
)
INSERT INTO AggregateLookup (name) 
	VALUES
	('minute')
	,('hourly')
	,('daily')
	,('weekly')
	,('monthly')
	,('quarterly')
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'PortfolioContent')
BEGIN
CREATE TABLE PortfolioContent
(
	portfolio_id INT NOT NULL
	,ticker_id INT NOT NULL
	,quantity INT NOT NULL
	PRIMARY KEY(portfolio_id, ticker_id)
)
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'StockInteraction')
BEGIN
CREATE TABLE StockInteraction
(
	stock_interaction_id INT IDENTITY(1,1) PRIMARY KEY
	,[datetime] DATETIME DEFAULT(SYSDATETIME())
	,customer_id INT FOREIGN KEY (customer_id) REFERENCES Customer(customer_id)
	,ticker_id INT FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
	,aggregate_period INT FOREIGN KEY (aggregate_period) REFERENCES AggregateLookup(aggregate_id)
)
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'PortfolioInteraction')
BEGIN
CREATE TABLE PortfolioInteraction 
(
	portfolio_interaction_id INT IDENTITY(1,1) PRIMARY KEY
	,[datetime] DATETIME DEFAULT(SYSDATETIME())
	,customer_id INT FOREIGN KEY (customer_id) REFERENCES Customer(customer_id)
	,portfolio_id INT FOREIGN KEY (portfolio_id) REFERENCES Portfolio(portfolio_id)
	,aggregate_period INT FOREIGN KEY (aggregate_period) REFERENCES AggregateLookup(aggregate_id)
)
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'AggsByMinute')
BEGIN
CREATE TABLE AggsByMinute
(
	agg_minute_id INT IDENTITY(1,1) PRIMARY KEY
	,ticker_id INT NOT NULL
	,date INT NOT NULL
	,time INT NOT NULL
	,[open] MONEY NOT NULL
	,high MONEY NOT NULL
	,low MONEY NOT NULL
	,[close] MONEY NOT NULL
	,volume REAL NOT NULL
	,split_factor REAL NOT NULL
	,earnings INT NOT NULL
	,dividends MONEY NOT NULL
	FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
)
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'AggsByHour')
BEGIN
CREATE TABLE AggsByHour(
 agg_hour_id INT IDENTITY(1,1) CONSTRAINT pk_hour_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [time] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'AggsByDay')
BEGIN
CREATE TABLE AggsByDay(
 agg_day_id INT IDENTITY(1,1) CONSTRAINT pk_day_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'AggsByWeek')
BEGIN
CREATE TABLE AggsByWeek(
 agg_week_id INT IDENTITY(1,1) CONSTRAINT pk_week_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);
END


IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'AggsByMonth')
BEGIN
CREATE TABLE AggsByMonth(
 agg_month_id INT IDENTITY(1,1) CONSTRAINT pk_month_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);
END

IF NOT EXISTS(SELECT * FROM sys.tables WHERE name = 'AggsByQuarter')
BEGIN
CREATE TABLE AggsByQuarter(
 agg_trimonth_id INT IDENTITY(1,1) CONSTRAINT pk_trimonth_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);
END

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'usp_populate_hourly_aggs')
DROP PROCEDURE usp_populate_hourly_aggs;
GO
CREATE PROCEDURE usp_populate_hourly_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_minute_id) AS OpeningID
				,max(agg_minute_id) AS ClosingID
				,ticker_id
				,DATE
				,TIME / 100 AS 'theHour'
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByMinute d
	GROUP BY ticker_id  
	,DATE 
	,TIME / 100
)
INSERT INTO AggsByHour
(
	ticker_id
	,[date]
	,[time]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.[date]
	,CTE.theHour
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByMinute m1 ON m1.agg_minute_id = CTE.OpeningID
	JOIN AggsByMinute m2 ON m2.agg_minute_id = CTE.ClosingID
END;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'usp_populate_daily_aggs')
DROP PROCEDURE usp_populate_daily_aggs;
GO
CREATE PROCEDURE usp_populate_daily_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_hour_id) AS OpeningID
				,max(agg_hour_id) AS ClosingID
				,ticker_id
				,[Date] AS theDay
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByHour d
	GROUP BY ticker_id  
	,DATE
)
INSERT INTO AggsByDay
(
	ticker_id
	,[date]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.theDay
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByHour m1 ON m1.agg_hour_id = CTE.OpeningID
	JOIN AggsByHour m2 ON m2.agg_hour_id = CTE.ClosingID
end;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'usp_populate_weekly_aggs')
DROP PROCEDURE usp_populate_weekly_aggs;
GO
CREATE PROCEDURE usp_populate_weekly_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_day_id) AS OpeningID
				,max(agg_day_id) AS ClosingID
				,ticker_id
				,min([date]) AS minDate
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByDay d
	GROUP BY ticker_id  
	,DATEPART(wk, CONVERT(DATE, CAST([date] AS CHAR(8))))
)
INSERT INTO AggsByWeek
(
	ticker_id
	,[date]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.minDate
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByHour m1 ON m1.agg_hour_id = CTE.OpeningID
	JOIN AggsByHour m2 ON m2.agg_hour_id = CTE.ClosingID
end;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'usp_populate_monthly_aggs')
DROP PROCEDURE usp_populate_monthly_aggs;
GO
CREATE PROCEDURE usp_populate_monthly_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_day_id) AS OpeningID
				,max(agg_day_id) AS ClosingID
				,ticker_id
				,min([date]) AS minDate
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByDay d
	GROUP BY ticker_id  
	,SUBSTRING(CAST([date] AS CHAR(8)), 1, 6)
)
INSERT INTO AggsByMonth
(
	ticker_id
	,[date]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.minDate
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByDay m1 ON m1.agg_day_id = CTE.OpeningID
	JOIN AggsByDay m2 ON m2.agg_day_id = CTE.ClosingID
end;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'usp_populate_quarterly_aggs')
DROP PROCEDURE usp_populate_quarterly_aggs;
GO
CREATE PROCEDURE usp_populate_quarterly_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_month_id) AS OpeningID
				,max(agg_month_id) AS ClosingID
				,ticker_id
				,min([date]) AS minDate
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByMonth d
	GROUP BY ticker_id  
    ,DATEPART(qq, CONVERT(DATE, CAST([date] AS CHAR(8))))
)
INSERT INTO AggsByQuarter
(
	ticker_id
	,[date]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.minDate
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByDay m1 ON m1.agg_day_id = CTE.OpeningID
	JOIN AggsByDay m2 ON m2.agg_day_id = CTE.ClosingID
end;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'usp_remove_aggs')
DROP PROCEDURE usp_remove_aggs;
GO
CREATE PROCEDURE usp_remove_aggs
AS
BEGIN
EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'

TRUNCATE TABLE AggsByHour;
DBCC CHECKIDENT ('AggsByHour', RESEED, 1)  
TRUNCATE TABLE AggsByDay;
DBCC CHECKIDENT ('AggsByDay', RESEED, 1)  
TRUNCATE TABLE AggsByWeek;
DBCC CHECKIDENT ('AggsByWeek', RESEED, 1)  
TRUNCATE TABLE AggsByMonth;
DBCC CHECKIDENT ('AggsByMonth', RESEED, 1)  
TRUNCATE TABLE AggsByQuarter;
DBCC CHECKIDENT ('AggsByQuarter', RESEED, 1)  

EXEC sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all'
END

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'usp_remove_all')
DROP PROCEDURE usp_remove_all;
GO
CREATE PROCEDURE usp_remove_all
AS
BEGIN
EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'

EXEC sp_MSforEachTable  'TRUNCATE TABLE ? DBCC CHECKIDENT (?, RESEED, 1)'

EXEC sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all'
END