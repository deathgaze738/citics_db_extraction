﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CITICSDBLoader
{
    public partial class frmMain : Form
    {
        private Boolean logging = false;
        private int batchCount;
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.Text = "CITICS Database Import Tool";
            this.txt_username.Text = "Fred";
            this.txt_password.Text = "Fred";
            this.txt_dbpath.Text = @".\sqlexpress";
            this.txt_directory.Text = @"C:\Users\Administrator\Desktop\dataanalyticsdata\DataAnalytics Data";
            this.txt_batch.Text = "1000";
            this.txt_name.Text = "db_citics";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_build_Click(object sender, EventArgs e)
        {
            buildDatabase();
        }

        private void writeToLog(string tag, string output)
        {
            if (logging || !tag.ToUpper().Equals("INFO"))
            {
                rtxt_output.AppendText($"{DateTime.Now} : {tag.ToUpper().PadRight(5, ' ')} : {output}\n");
            }
            rtxt_output.Refresh();
            rtxt_output.ScrollToCaret();
        }

        private void runSQLMSQuery(SqlConnection dbConn, string path)
        {
                String buildDb = File.ReadAllText(path);
                SqlCommand newDatabase = new SqlCommand(buildDb, dbConn);
                string sqlBatch = string.Empty;
                //Very sketchy hack that I found online to handle SQLServer Management Studio syntax.
                try
                {
                    foreach (string line in buildDb.Split(new string[2] { "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (line.ToUpperInvariant().Trim() == "GO")
                        {
                            newDatabase.CommandText = sqlBatch;
                            newDatabase.ExecuteNonQuery();
                            sqlBatch = string.Empty;
                        }
                        else
                        {
                            sqlBatch += line + "\n";
                        }
                    }
                }
                catch(SqlException e)
                {
                    writeToLog("ERROR", e.ToString());
                }
        }

        private Tuple<Dictionary<string, int>, int> mappedTickers(SqlConnection dbConn)
        {
            var tickerDic = new Dictionary<string, int>();
            SqlCommand cmdTickers = new SqlCommand("SELECT ticker_id, symbol FROM Ticker", dbConn);
            SqlCommand cmdMaxId = new SqlCommand("SELECT MAX(ticker_id) FROM Ticker", dbConn);
            var dt = new DataTable();
            var s = cmdMaxId.ExecuteScalar().ToString();

            int maxId = int.Parse(String.IsNullOrEmpty(s) ? "0": s);
            cmdMaxId.Connection = null;
            dt.Load(cmdTickers.ExecuteReader());
            foreach(DataRow row in dt.Rows) 
            {
                Object[] items = row.ItemArray;
                tickerDic.Add((string)items[1], (int)items[0]);
            }
            return new Tuple<Dictionary<string, int>, int>(tickerDic, maxId);
        }

        private Dictionary<string, int> mapTickers(SqlConnection dbConn, string directory, IOrderedEnumerable<KeyValuePair<string, List<string>>> fileSetEnum)
        {
            //Set up and insert tickers.
            writeToLog("LOG", "Mapping and Inserting Tickers...");
            Tuple<Dictionary<string, int>, int> mappedTickerResult = mappedTickers(dbConn);
            Dictionary<string, int> indexMap = mappedTickerResult.Item1;
            int count = mappedTickerResult.Item2;
            int newTickers = 0;

            DataTable tickerInsert = new DataTable();
            tickerInsert.Columns.Add("symbol");
            tickerInsert.Columns.Add("full_name");
            
            foreach (var mapping in fileSetEnum)
            {
                if (!indexMap.TryGetValue(mapping.Key, out int value))
                {
                    writeToLog("INFO", $"Found new ticker: {mapping.Key}");
                    tickerInsert.Rows.Add(mapping.Key, "Test");
                    indexMap.Add(mapping.Key, ++count);
                }
                newTickers++;
            }

            using (var sqlBulk = new SqlBulkCopy(dbConn))
            {
                sqlBulk.ColumnMappings.Add("full_name", "full_name");
                sqlBulk.ColumnMappings.Add("symbol", "symbol");
                sqlBulk.DestinationTableName = "Ticker";
                sqlBulk.WriteToServer(tickerInsert);
            }
            writeToLog("LOG", $"Completed mapping and inserting {newTickers} new tickers.");
            return indexMap;
        }

        private void insertData(IOrderedEnumerable<KeyValuePair<string, List<string>>> fileSetEnum, Dictionary<string, int> indexMap, SqlConnection dbConn)
        {
            //Inserting Stocks
            writeToLog("LOG", "Mapping and Inserting Stocks...");

            DataTable stockInsert = new DataTable();
            stockInsert.Columns.Add("ticker_id", typeof(int));
            stockInsert.Columns.Add("date", typeof(int));
            stockInsert.Columns.Add("time", typeof(int));
            stockInsert.Columns.Add("open", typeof(decimal));
            stockInsert.Columns.Add("high", typeof(decimal));
            stockInsert.Columns.Add("low", typeof(decimal));
            stockInsert.Columns.Add("close", typeof(decimal));
            stockInsert.Columns.Add("volume");
            stockInsert.Columns.Add("split_factor", typeof(decimal));
            stockInsert.Columns.Add("earnings", typeof(decimal));
            stockInsert.Columns.Add("dividends", typeof(decimal));
            int flushCount = 0;
            int totalRecords = 0;
            foreach (var mapping in fileSetEnum)
            {
                int tickerId;
                indexMap.TryGetValue(mapping.Key, out tickerId);
                List<String> files = mapping.Value;
                writeToLog("INFO", $"Inserting ticker {mapping.Key} with {files.Count} files.");

                foreach (var file in files)
                {
                    string[] lines = File.ReadAllLines(file);
                    foreach (var line in lines)
                    {
                        string[] fields = line.Split(',');
                        stockInsert.Rows.Add(tickerId,
                            int.Parse(fields[0]),
                            int.Parse(fields[1]),
                            decimal.Parse(fields[2]),
                            decimal.Parse(fields[3]),
                            decimal.Parse(fields[4]),
                            decimal.Parse(fields[5]),
                            decimal.Parse(fields[6], System.Globalization.NumberStyles.Float),
                            decimal.Parse(fields[7]),
                            decimal.Parse(fields[8]),
                            fields[9]);
                    }
                    flushCount++;
                    if (flushCount % batchCount == 0)
                    {
                        using (SqlBulkCopy sqlBulk = new SqlBulkCopy(dbConn))
                        {
                            sqlBulk.ColumnMappings.Add("ticker_id", "ticker_id");
                            sqlBulk.ColumnMappings.Add("date", "date");
                            sqlBulk.ColumnMappings.Add("time", "time");
                            sqlBulk.ColumnMappings.Add("open", "open");
                            sqlBulk.ColumnMappings.Add("high", "high");
                            sqlBulk.ColumnMappings.Add("low", "low");
                            sqlBulk.ColumnMappings.Add("close", "close");
                            sqlBulk.ColumnMappings.Add("volume", "volume");
                            sqlBulk.ColumnMappings.Add("split_factor", "split_factor");
                            sqlBulk.ColumnMappings.Add("earnings", "earnings");
                            sqlBulk.ColumnMappings.Add("dividends", "dividends");
                            sqlBulk.DestinationTableName = "AggsByMinute";
                            sqlBulk.WriteToServer(stockInsert);
                            totalRecords += stockInsert.Rows.Count;
                            stockInsert.Clear();
                            writeToLog("INFO", $"{flushCount} files have been processed with {totalRecords} rows inserted.");
                        }
                    }
                }
            }
            //Clean up flush
            using (SqlBulkCopy sqlBulk = new SqlBulkCopy(dbConn))
            {
                sqlBulk.ColumnMappings.Add("ticker_id", "ticker_id");
                sqlBulk.ColumnMappings.Add("date", "date");
                sqlBulk.ColumnMappings.Add("time", "time");
                sqlBulk.ColumnMappings.Add("open", "open");
                sqlBulk.ColumnMappings.Add("high", "high");
                sqlBulk.ColumnMappings.Add("low", "low");
                sqlBulk.ColumnMappings.Add("close", "close");
                sqlBulk.ColumnMappings.Add("volume", "volume");
                sqlBulk.ColumnMappings.Add("split_factor", "split_factor");
                sqlBulk.ColumnMappings.Add("earnings", "earnings");
                sqlBulk.ColumnMappings.Add("dividends", "dividends");
                sqlBulk.DestinationTableName = "AggsByMinute";
                sqlBulk.WriteToServer(stockInsert);
                stockInsert.Clear();
                writeToLog("INFO", $"{flushCount} files have been processed with {totalRecords} rows inserted.");
            }
            writeToLog("LOG", "Completed inserting stocks into database.");
        }

        private void insertAggregates(SqlConnection dbConn)
        {
            writeToLog("LOG", "Creating Aggregations Initalized.");
            var cmd = new SqlCommand("usp_populate_hourly_aggs", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            cmd = new SqlCommand("usp_populate_daily_aggs", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            cmd = new SqlCommand("usp_populate_weekly_aggs", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            cmd = new SqlCommand("usp_populate_monthly_aggs", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            cmd = new SqlCommand("usp_populate_quarter_aggs", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            writeToLog("LOG", "Completed Aggreegations");
        }

        private SqlConnection getConnection()
        {
            SqlConnection dbConn;
            string dbName = this.txt_name.Text;
            string dbUsername = this.txt_username.Text;
            string dbPassword = this.txt_password.Text;
            string dbPath = this.txt_dbpath.Text;

            if (this.chkbox_authentication.Checked)
            {
                dbConn = new SqlConnection($"server={dbPath}; database={dbName}; user={dbUsername}; password={dbPassword}");
            }
            else
            {
                dbConn = new SqlConnection($"server={dbPath}; database={dbName};");
            }
            return dbConn;
        }

        private void buildDatabase()
        {
            string directory = this.txt_directory.Text;

            logging = this.chkbox_verbose.Checked;
            batchCount = int.Parse(this.txt_batch.Text);

            var startTime = DateTime.Now;

            writeToLog("log", $"Initializing Data Import to '{this.txt_name.Text}' from directory '{directory}'.");
            SqlConnection dbConn = getConnection();
            
            try
            {
                dbConn.Open();
                SqlCommand testCmd = new SqlCommand("SELECT @@Version", dbConn);
                testCmd.ExecuteNonQuery();
                writeToLog("INFO", $"Connection to {dbConn.Database} successful!");
            }
            catch
            {
                writeToLog("ERROR", "Database does not exist. Please enter valid connection parameters.");
                return;
            }

            try
            {
                runSQLMSQuery(dbConn, "build.sql");
                SqlCommand nukeAggs = new SqlCommand("usp_remove_aggs", dbConn);
            }
            catch(Exception e)
            {
                writeToLog("ERROR", e.ToString());
                return;
            }

            Dictionary<string, List<string>> tickerSet = getTickerFiles(directory);
            IOrderedEnumerable<KeyValuePair<string, List<string>>> tickerSetEnum = tickerSet.AsEnumerable().OrderBy(s => s.Key);
            Dictionary<string, int> indexMap = mapTickers(dbConn, directory, tickerSetEnum);

            insertData(tickerSetEnum, indexMap, dbConn);

            insertAggregates(dbConn);

            var endTime = DateTime.Now;
            dbConn.Close();
            writeToLog("LOG", $"Took {(endTime - startTime).TotalSeconds} seconds to insert.");
        }

        unsafe static Dictionary<string, List<string>> getTickerFiles(string path)
        {
            Dictionary<string, List<string>> tickerSet = new Dictionary<string, List<string>>();
            string[] files = { };
            try
            {
                files = Directory.GetFiles(path, "*.csv", SearchOption.AllDirectories);
            }
            catch
            {
                
            }
            foreach (var file in files)
            {
                IntPtr strp = Marshal.StringToCoTaskMemAnsi(Path.GetFileNameWithoutExtension(file));
                strp += 6;
                string str = Marshal.PtrToStringAnsi((IntPtr)strp);
                if (!tickerSet.ContainsKey(str))
                {
                    tickerSet.Add(str, new List<string>());
                }
                List<string> mList;
                tickerSet.TryGetValue(str, out mList);
                mList.Add(Path.GetFullPath(file));
            }
            return tickerSet;
        }

        private void chkbox_authentication_CheckedChanged(object sender, EventArgs e)
        {
            this.label_username.Enabled = !this.label_username.Enabled;
            this.txt_username.Enabled = !this.txt_username.Enabled;
            this.label_passwords.Enabled = !this.label_passwords.Enabled;
            this.txt_password.Enabled = !this.txt_password.Enabled;
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            folderBrowserDialog_directory.ShowDialog();
            this.txt_directory.Text = folderBrowserDialog_directory.SelectedPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection dbConn = getConnection();
            try
            {
                dbConn.Open();
                SqlCommand testCmd = new SqlCommand("SELECT @@Version", dbConn);
                testCmd.ExecuteNonQuery();
                writeToLog("INFO", $"Connection to {dbConn.Database} successful!");
            }
            catch
            {
                writeToLog("ERROR", "Database does not exist. Please enter valid connection parameters.");
                return;
            }
            writeToLog("LOG", $"Emptying {dbConn.Database}.");
            SqlCommand nukeDb = new SqlCommand("usp_remove_all", dbConn);
            nukeDb.ExecuteNonQuery();
            writeToLog("LOG", $"Completed emptying {dbConn.Database}.");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
