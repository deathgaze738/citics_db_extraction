if  exists(select * from sys.databases d where d.name='db_citics')
begin
use master;
alter database db_citics set single_user with rollback immediate;
drop database db_citics
end

GO
CREATE DATABASE db_citics 
GO
use db_citics;
GO

CREATE TABLE Ticker
(
	ticker_id INT IDENTITY(1,1) PRIMARY KEY
	,symbol VARCHAR(99) NOT NULL
	,full_name VARCHAR(99) NOT NULL
)

CREATE TABLE Customer
(
	customer_id INT IDENTITY(1,1) PRIMARY KEY
	,email_address VARCHAR(255) UNIQUE NOT NULL
	,password VARCHAR(255) NOT NULL
	,full_name VARCHAR(255) NOT NULL
	,last_login DATETIME DEFAULT(SYSDATETIME()) NOT NULL
)

CREATE TABLE Portfolio
(
	portfolio_id INT IDENTITY(1,1) PRIMARY KEY
	,owner_id INT FOREIGN KEY (owner_id) REFERENCES Customer(customer_id)
	,name VARCHAR(255) NOT NULL
	,isPrivate BIT NOT NULL DEFAULT(1)
)

CREATE TABLE AggregateLookup
(
	aggregate_id INT IDENTITY(1,1) PRIMARY KEY
	,name VARCHAR(99) NOT NULL
)
INSERT INTO AggregateLookup (name) 
	VALUES
	('minute')
	,('hourly')
	,('daily')
	,('weekly')
	,('monthly')
	,('quarterly')

CREATE TABLE PortfolioContent
(
	portfolio_id INT NOT NULL
	,ticker_id INT NOT NULL
	,quantity INT NOT NULL
	PRIMARY KEY(portfolio_id, ticker_id)
)

CREATE TABLE StockInteraction
(
	stock_interaction_id INT IDENTITY(1,1) PRIMARY KEY
	,[datetime] DATETIME DEFAULT(SYSDATETIME())
	,customer_id INT FOREIGN KEY (customer_id) REFERENCES Customer(customer_id)
	,ticker_id INT FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
	,aggregate_period INT FOREIGN KEY (aggregate_period) REFERENCES AggregateLookup(aggregate_id)
)

CREATE TABLE PortfolioInteraction 
(
	portfolio_interaction_id INT IDENTITY(1,1) PRIMARY KEY
	,[datetime] DATETIME DEFAULT(SYSDATETIME())
	,customer_id INT FOREIGN KEY (customer_id) REFERENCES Customer(customer_id)
	,portfolio_id INT FOREIGN KEY (portfolio_id) REFERENCES Portfolio(portfolio_id)
	,aggregate_period INT FOREIGN KEY (aggregate_period) REFERENCES AggregateLookup(aggregate_id)
)

CREATE TABLE AggsByMinute
(
	agg_minute_id INT IDENTITY(1,1) PRIMARY KEY
	,ticker_id INT NOT NULL
	,date INT NOT NULL
	,time INT NOT NULL
	,[open] MONEY NOT NULL
	,high MONEY NOT NULL
	,low MONEY NOT NULL
	,[close] MONEY NOT NULL
	,volume REAL NOT NULL
	,split_factor REAL NOT NULL
	,earnings INT NOT NULL
	,dividends MONEY NOT NULL
	FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)

)

CREATE TABLE AggsByHour(
 agg_hour_id INT IDENTITY(1,1) CONSTRAINT pk_hour_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [time] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);

GO

CREATE TABLE AggsByDay(
 agg_day_id INT IDENTITY(1,1) CONSTRAINT pk_day_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);
GO

CREATE TABLE AggsByWeek(
 agg_week_id INT IDENTITY(1,1) CONSTRAINT pk_week_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);
GO

CREATE TABLE AggsByMonth(
 agg_month_id INT IDENTITY(1,1) CONSTRAINT pk_month_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);
GO

CREATE TABLE AggsByQuarter(
 agg_trimonth_id INT IDENTITY(1,1) CONSTRAINT pk_trimonth_aggs PRIMARY KEY
, ticker_id int
, [date] int
, [open] money
, high money
, low money
, [close] money
, volume  real
, split_factor real
, earnings int
, dividends MONEY
FOREIGN KEY (ticker_id) REFERENCES Ticker(ticker_id)
);
GO

CREATE PROCEDURE usp_populate_hourly_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_minute_id) AS OpeningID
				,max(agg_minute_id) AS ClosingID
				,ticker_id
				,DATE
				,TIME / 100 AS 'theHour'
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByMinute d
	GROUP BY ticker_id  
	,DATE 
	,TIME / 100
)
INSERT INTO AggsByHour
(
	ticker_id
	,[date]
	,[time]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.[date]
	,CTE.theHour
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByMinute m1 ON m1.agg_minute_id = CTE.OpeningID
	JOIN AggsByMinute m2 ON m2.agg_minute_id = CTE.ClosingID
end;
GO

CREATE PROCEDURE usp_populate_daily_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_hour_id) AS OpeningID
				,max(agg_hour_id) AS ClosingID
				,ticker_id
				,[Date] AS theDay
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByHour d
	GROUP BY ticker_id  
	,DATE
)
INSERT INTO AggsByDay
(
	ticker_id
	,[date]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.theDay
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByHour m1 ON m1.agg_hour_id = CTE.OpeningID
	JOIN AggsByHour m2 ON m2.agg_hour_id = CTE.ClosingID
end;
GO

CREATE PROCEDURE usp_populate_weekly_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_day_id) AS OpeningID
				,max(agg_day_id) AS ClosingID
				,ticker_id
				,min([date]) AS minDate
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByDay d
	GROUP BY ticker_id  
	,DATEPART(wk, CONVERT(DATE, CAST([date] AS CHAR(8))))
)
INSERT INTO AggsByWeek
(
	ticker_id
	,[date]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.minDate
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByHour m1 ON m1.agg_hour_id = CTE.OpeningID
	JOIN AggsByHour m2 ON m2.agg_hour_id = CTE.ClosingID
end;
GO

CREATE PROCEDURE usp_populate_monthly_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_day_id) AS OpeningID
				,max(agg_day_id) AS ClosingID
				,ticker_id
				,min([date]) AS minDate
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByDay d
	GROUP BY ticker_id  
	,SUBSTRING(CAST([date] AS CHAR(8)), 1, 6)
)
INSERT INTO AggsByMonth
(
	ticker_id
	,[date]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.minDate
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByDay m1 ON m1.agg_day_id = CTE.OpeningID
	JOIN AggsByDay m2 ON m2.agg_day_id = CTE.ClosingID
end;
GO

CREATE PROCEDURE usp_populate_quarter_aggs
AS
BEGIN
WITH CTE AS
(
		SELECT min(agg_month_id) AS OpeningID
				,max(agg_month_id) AS ClosingID
				,ticker_id
				,min([date]) AS minDate
				,max(High) AS maxHi
				,min(Low) AS minLow
				,Sum(Volume) AS 'sumVolume'
				,avg(Split_Factor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AggsByMonth d
	GROUP BY ticker_id  
    ,DATEPART(qq, CONVERT(DATE, CAST([date] AS CHAR(8))))
)
INSERT INTO AggsByQuarter
(
	ticker_id
	,[date]
	,[open]
	,high
	,low
	,[close]
	,volume
	,split_factor
	,earnings
	,dividends
) 
SELECT
	CTE.ticker_id
	,CTE.minDate
	,m1.[open]
	,maxHi
	,minLow
	,m2.[close]
	,sumVolume
	,avgSplitFactor
	,sumEarnings
	,sumDividends
FROM CTE
	JOIN AggsByDay m1 ON m1.agg_day_id = CTE.OpeningID
	JOIN AggsByDay m2 ON m2.agg_day_id = CTE.ClosingID
end;
GO