﻿namespace CITICSDBLoader
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_dbname = new System.Windows.Forms.Label();
            this.label_directory = new System.Windows.Forms.Label();
            this.label_username = new System.Windows.Forms.Label();
            this.label_passwords = new System.Windows.Forms.Label();
            this.chkbox_authentication = new System.Windows.Forms.CheckBox();
            this.txt_dbpath = new System.Windows.Forms.TextBox();
            this.txt_directory = new System.Windows.Forms.TextBox();
            this.txt_username = new System.Windows.Forms.TextBox();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog_directory = new System.Windows.Forms.FolderBrowserDialog();
            this.btn_browse = new System.Windows.Forms.Button();
            this.chkbox_verbose = new System.Windows.Forms.CheckBox();
            this.btn_build = new System.Windows.Forms.Button();
            this.rtxt_output = new System.Windows.Forms.RichTextBox();
            this.btn_clear = new System.Windows.Forms.Button();
            this.lbl_batch = new System.Windows.Forms.Label();
            this.txt_batch = new System.Windows.Forms.TextBox();
            this.lbl_dbname = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label_dbname
            // 
            this.label_dbname.AutoSize = true;
            this.label_dbname.Location = new System.Drawing.Point(9, 11);
            this.label_dbname.Name = "label_dbname";
            this.label_dbname.Size = new System.Drawing.Size(55, 15);
            this.label_dbname.TabIndex = 0;
            this.label_dbname.Text = "DB Path:";
            this.label_dbname.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_directory
            // 
            this.label_directory.AutoSize = true;
            this.label_directory.Location = new System.Drawing.Point(360, 11);
            this.label_directory.Name = "label_directory";
            this.label_directory.Size = new System.Drawing.Size(58, 15);
            this.label_directory.TabIndex = 1;
            this.label_directory.Text = "Directory:";
            // 
            // label_username
            // 
            this.label_username.AutoSize = true;
            this.label_username.Enabled = false;
            this.label_username.Location = new System.Drawing.Point(14, 93);
            this.label_username.Name = "label_username";
            this.label_username.Size = new System.Drawing.Size(36, 15);
            this.label_username.TabIndex = 2;
            this.label_username.Text = "User:";
            // 
            // label_passwords
            // 
            this.label_passwords.AutoSize = true;
            this.label_passwords.Enabled = false;
            this.label_passwords.Location = new System.Drawing.Point(360, 90);
            this.label_passwords.Name = "label_passwords";
            this.label_passwords.Size = new System.Drawing.Size(64, 15);
            this.label_passwords.TabIndex = 3;
            this.label_passwords.Text = "Password:";
            // 
            // chkbox_authentication
            // 
            this.chkbox_authentication.AutoSize = true;
            this.chkbox_authentication.Location = new System.Drawing.Point(18, 64);
            this.chkbox_authentication.Name = "chkbox_authentication";
            this.chkbox_authentication.Size = new System.Drawing.Size(138, 19);
            this.chkbox_authentication.TabIndex = 4;
            this.chkbox_authentication.Text = "Use Authentication?";
            this.chkbox_authentication.UseVisualStyleBackColor = true;
            this.chkbox_authentication.CheckedChanged += new System.EventHandler(this.chkbox_authentication_CheckedChanged);
            // 
            // txt_dbpath
            // 
            this.txt_dbpath.Location = new System.Drawing.Point(77, 8);
            this.txt_dbpath.Name = "txt_dbpath";
            this.txt_dbpath.Size = new System.Drawing.Size(269, 20);
            this.txt_dbpath.TabIndex = 5;
            // 
            // txt_directory
            // 
            this.txt_directory.Location = new System.Drawing.Point(430, 6);
            this.txt_directory.Name = "txt_directory";
            this.txt_directory.Size = new System.Drawing.Size(268, 20);
            this.txt_directory.TabIndex = 6;
            // 
            // txt_username
            // 
            this.txt_username.Enabled = false;
            this.txt_username.Location = new System.Drawing.Point(77, 90);
            this.txt_username.Name = "txt_username";
            this.txt_username.Size = new System.Drawing.Size(269, 20);
            this.txt_username.TabIndex = 7;
            // 
            // txt_password
            // 
            this.txt_password.Enabled = false;
            this.txt_password.Location = new System.Drawing.Point(430, 88);
            this.txt_password.Name = "txt_password";
            this.txt_password.Size = new System.Drawing.Size(350, 20);
            this.txt_password.TabIndex = 8;
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(705, 6);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(75, 23);
            this.btn_browse.TabIndex = 9;
            this.btn_browse.Text = "Browse...";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // chkbox_verbose
            // 
            this.chkbox_verbose.AutoSize = true;
            this.chkbox_verbose.Location = new System.Drawing.Point(360, 39);
            this.chkbox_verbose.Name = "chkbox_verbose";
            this.chkbox_verbose.Size = new System.Drawing.Size(129, 19);
            this.chkbox_verbose.TabIndex = 11;
            this.chkbox_verbose.Text = "Verbose Logging?";
            this.chkbox_verbose.UseVisualStyleBackColor = true;
            // 
            // btn_build
            // 
            this.btn_build.Location = new System.Drawing.Point(14, 125);
            this.btn_build.Name = "btn_build";
            this.btn_build.Size = new System.Drawing.Size(376, 33);
            this.btn_build.TabIndex = 14;
            this.btn_build.Text = "Build Database";
            this.btn_build.UseVisualStyleBackColor = true;
            this.btn_build.Click += new System.EventHandler(this.btn_build_Click);
            // 
            // rtxt_output
            // 
            this.rtxt_output.Location = new System.Drawing.Point(12, 164);
            this.rtxt_output.Name = "rtxt_output";
            this.rtxt_output.Size = new System.Drawing.Size(768, 277);
            this.rtxt_output.TabIndex = 15;
            this.rtxt_output.Text = "";
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(396, 125);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(381, 33);
            this.btn_clear.TabIndex = 16;
            this.btn_clear.Text = "Clear Database";
            this.btn_clear.UseMnemonic = false;
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbl_batch
            // 
            this.lbl_batch.AutoSize = true;
            this.lbl_batch.Location = new System.Drawing.Point(499, 41);
            this.lbl_batch.Name = "lbl_batch";
            this.lbl_batch.Size = new System.Drawing.Size(91, 15);
            this.lbl_batch.TabIndex = 17;
            this.lbl_batch.Text = "File Batch Size:";
            // 
            // txt_batch
            // 
            this.txt_batch.Location = new System.Drawing.Point(609, 38);
            this.txt_batch.Name = "txt_batch";
            this.txt_batch.Size = new System.Drawing.Size(168, 20);
            this.txt_batch.TabIndex = 18;
            this.txt_batch.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lbl_dbname
            // 
            this.lbl_dbname.AutoSize = true;
            this.lbl_dbname.Location = new System.Drawing.Point(12, 41);
            this.lbl_dbname.Name = "lbl_dbname";
            this.lbl_dbname.Size = new System.Drawing.Size(44, 15);
            this.lbl_dbname.TabIndex = 19;
            this.lbl_dbname.Text = "Name:";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(77, 38);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(269, 20);
            this.txt_name.TabIndex = 20;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 450);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.lbl_dbname);
            this.Controls.Add(this.txt_batch);
            this.Controls.Add(this.lbl_batch);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.rtxt_output);
            this.Controls.Add(this.btn_build);
            this.Controls.Add(this.chkbox_verbose);
            this.Controls.Add(this.btn_browse);
            this.Controls.Add(this.txt_password);
            this.Controls.Add(this.txt_username);
            this.Controls.Add(this.txt_directory);
            this.Controls.Add(this.txt_dbpath);
            this.Controls.Add(this.chkbox_authentication);
            this.Controls.Add(this.label_passwords);
            this.Controls.Add(this.label_username);
            this.Controls.Add(this.label_directory);
            this.Controls.Add(this.label_dbname);
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_dbname;
        private System.Windows.Forms.Label label_directory;
        private System.Windows.Forms.Label label_username;
        private System.Windows.Forms.Label label_passwords;
        private System.Windows.Forms.CheckBox chkbox_authentication;
        private System.Windows.Forms.TextBox txt_dbpath;
        private System.Windows.Forms.TextBox txt_directory;
        private System.Windows.Forms.TextBox txt_username;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog_directory;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.CheckBox chkbox_verbose;
        private System.Windows.Forms.Button btn_build;
        private System.Windows.Forms.RichTextBox rtxt_output;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.Label lbl_batch;
        private System.Windows.Forms.TextBox txt_batch;
        private System.Windows.Forms.Label lbl_dbname;
        private System.Windows.Forms.TextBox txt_name;
    }
}